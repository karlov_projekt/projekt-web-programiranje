<!DOCTYPE html>
<?php
    if(isset($_POST['Submit'])){
        $conn = mysqli_connect("localhost", "root", "", "project_web");
		
		// Check connection
		if($conn === false){
			die("ERROR: Could not connect. "
				. mysqli_connect_error());
		}

		
		// Taking  values from the form data(input)
		$username = $_POST['username'];
		$password = $_POST['password'];
        
        //Check username and pass
        $sql = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_row($result);
        //Needs better alert system
        if(!$row) {
            header( "Location: login.php" );
            
            return;
        } 

        
        if(mysqli_query($conn, $sql)){
            header( "Location: home.html" );
        } else{
            echo "ERROR: Hush! Sorry $sql. "
                . mysqli_error($conn);
        }
        
        // Close connection
        mysqli_close($conn);
    }
?>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
    integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="styles.css">

  <title>F1-Login</title>
</head>

<body>
  <div class="container">
    <div class="d-flex justify-content-center h-100">
      <div class="card">
        <div class="card-header">
          <h2>Sign In</h2>
          <img src="assets\formula1-logo.png" class="logo">
        </div>
        <div class="card-body">
          <form action="#" method="post">
            <div class="input-group form-group">
              <div class="input-group-prepend">
                <span class="input-group-text justify-content-center"><i class="fas fa-user"></i></span>
              </div>
              <input type="text" class="form-control" placeholder="Username" name="username" id="username">

            </div>
            <div class="input-group form-group">
              <div class="input-group-prepend">
                <span class="input-group-text justify-content-center"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" class="form-control" placeholder="Password" name="password" id="password">
            </div>
            <div class="form-group">
              <input type="submit" name="Submit" class="btn float-right login_btn">
            </div>
          </form>
        </div>
        <div class="card-footer">
          <div class="d-flex justify-content-center links">
            <span>Don't have an account?</span>
            <a href="register.php">Register</a>
          </div>
        </div>
      </div>
      <div class="d-flex justify-content-end social_icon" id="font-awesome-colors">
        <span><a href="https://www.facebook.com/Formula1/"><i class="fab fa-facebook-square"></i></a></span>
        <span><a href="https://twitter.com/f1"><i class="fab fa-twitter-square"></i></a></span>
        <span><a href="https://www.youtube.com/channel/UCB_qr75-ydFVKSF9Dmo6izg"><i
              class="fab fa-youtube-square"></i></a></span>
      </div>
    </div>
  </div>
</body>

</html>