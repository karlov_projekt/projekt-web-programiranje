var standings = [];
window.onload = function () {
	const options = {
		method: 'GET',
		headers: {
			'X-RapidAPI-Key': 'e55d7206ccmshe56d83d284035dbp1a7030jsn2b8963ebd0e6',
		    'X-RapidAPI-Host': 'fia-formula-1-championship-statistics.p.rapidapi.com'
		}
	};

	fetch('https://fia-formula-1-championship-statistics.p.rapidapi.com/api/teams', options)
		.then(response => response.json())
		.then(response => {
			standings = response
			fillStandingsTable()
		})
		.catch(err => console.error(err));
	
}

function fillStandingsTable() {
	//Create a HTML Table element.
	var table = document.createElement("table");
	table.classList=['tg']
	table.border = "1";

	//Add the header row.
	var row = table.insertRow(-1);

	//Add the header cells.
	var headerCell = document.createElement("TH");
	headerCell.innerHTML = "Driver";
	row.appendChild(headerCell);

	headerCell = document.createElement("TH");
	headerCell.innerHTML = "Ranking";
	row.appendChild(headerCell);

	headerCell = document.createElement("TH");
	headerCell.innerHTML = "Points";
	row.appendChild(headerCell);

	//Add the data rows.
	for (var i = 0; i < 10; i++) {
		//Add the data row.
		var row = table.insertRow(-1);
		row.id = i;

		//Add the data cells.

        cell = row.insertCell(-1);
		cell.innerHTML = standings.teams[i].drivers[0]['lastname'] + ', ' + standings.teams[i].drivers[1]['lastname'];

		cell = row.insertCell(-1);
		cell.innerHTML = standings.teams[i].rank['standing'];
        
        var cell = row.insertCell(-1);
		cell.innerHTML = standings.teams[i].points['pts'];

        //
		
		
	}

	var dvTable = document.getElementById('standingsTable');
	dvTable.innerHTML = "";
	dvTable.appendChild(table);
}

